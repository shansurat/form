import ad from 'html-loader!./forms/account-deletion.form.html'
import la from 'html-loader!./forms/locked-account.form.html'
import br from 'html-loader!./forms/bug-report.form.html'
import pi from 'html-loader!./forms/partner-integration.form.html'
import mi from 'html-loader!./forms/media-inquiry.form.html'
import vpfr from 'html-loader!./forms/verified-public-figure-request.form.html'

import $ from 'jquery'

import {
    MDCTextField
} from '@material/textfield';

import {
    MDCSelect
} from '@material/select';

import {
    MDCFormField
} from '@material/form-field';
import {
    MDCRadio
} from '@material/radio';
import {
    MDCRipple
} from '@material/ripple';
import {
    MDCList
} from '@material/list';
import {
    MDCSlider
} from '@material/slider';


$(() => {
    new MDCRipple($('#clear')[0])
    new MDCRipple($('#submit')[0])

    const slider = new MDCSlider($('.mdc-slider')[0])
    slider.listen('MDCSlider:input', () => {

        $('#form').width(Math.ceil(slider.value) + '%')
    })




    generateForm('ad')

    const form_select = new MDCSelect($('#form-select')[0])
    const form_select__list = new MDCList($('#form-select__menu>.mdc-list')[0])

    form_select__list.listElements.map((listItemEl) => new MDCRipple(listItemEl))
    form_select.listen('MDCSelect:change', () => {
        $('#form-select__menu>.mdc-list>li').show().eq(form_select.selectedIndex).hide()

        generateForm(form_select.value)
    })

    $('#clear').click(() => generateForm(form_select.value))
})

function generateForm(form) {
    $('#submit').attr('form', form)

    if (form == 'ad') {
        $('#form>#container').html($.parseHTML(ad))
        new MDCTextField($('#username')[0])
        new MDCTextField($('#email')[0])
        new MDCTextField($('#comment')[0])
    } else if (form == 'la') {
        $('#form>#container').html($.parseHTML(la))
        new MDCTextField($('#username')[0])
        new MDCTextField($('#email')[0])
        new MDCTextField($('#comment')[0])
    } else if (form == 'br') {
        $('#form>#container').html($.parseHTML(br))
        new MDCTextField($('#username')[0])
        new MDCTextField($('#email')[0])
        new MDCSelect($('#device-type')[0])
        new MDCTextField($('#comment')[0])
    } else if (form == 'pi') {
        $('#form>#container').html($.parseHTML(pi))
        new MDCTextField($('#company')[0])
        new MDCTextField($('#website')[0])
        new MDCTextField($('#role')[0])
        new MDCTextField($('#first-name')[0])
        new MDCTextField($('#last-name')[0])
        new MDCTextField($('#email')[0])
        new MDCTextField($('#phone')[0])
        new MDCTextField($('#handle')[0])

        const comment_frequency__yes = new MDCFormField($('#comment-frequency-yes')[0])
        const comment_frequency__no = new MDCFormField($('#comment-frequency-no')[0])
        comment_frequency__yes.input = new MDCRadio($('#comment-frequency-yes>.mdc-radio')[0])
        comment_frequency__no.input = new MDCRadio($('#comment-frequency-no>.mdc-radio')[0])
    } else if (form == 'mi') {
        $('#form>#container').html($.parseHTML(mi))
        new MDCTextField($('#first-name')[0])
        new MDCTextField($('#last-name')[0])
        new MDCTextField($('#email')[0])
        new MDCTextField($('#phone')[0])
        new MDCTextField($('#media-outlet')[0])
        new MDCTextField($('#request-description')[0])
        new MDCTextField($('#deadline-datetime')[0])
        new MDCTextField($('#air-date')[0])

        const role = new MDCSelect($('#role')[0])
        const outlet_type = new MDCSelect($('#outlet-type')[0])
    } else if (form == 'vfpr') {
        $('#form>#container').html($.parseHTML(vpfr))
        new MDCTextField($('#handle')[0])
        new MDCTextField($('#email')[0])
        new MDCTextField($('#first-name')[0])
        new MDCTextField($('#last-name')[0])
        new MDCTextField($('#account-link-1')[0])
        new MDCTextField($('#account-link-2')[0])

        const account_type = new MDCSelect($('#account-type')[0])

    }
}